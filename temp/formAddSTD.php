<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>HONG TAO</title>
    <!-- sweet alert  -->
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <h4>ระบบเพิ่มข้อมูลนักศึกษา
                    <hr>
                </h4>
                <?php
                //ถ้ามีการส่งพารามิเตอร์ method get และ  method get ชือ act = add จะแสดงฟอร์มเพิ่มข้อมูล
                if (isset($_GET['act']) && $_GET['act'] == 'add') { ?>
                <h3>PHP PDO ฟอร์มเพิ่มข้อมูลนักศึกษา </h3>
                <form method="post">
                    <div class="row mb-3">
                        <div class="col">
                            เลือกโปรแกรมวิชา
                            <select name="std_program" class="form-control" required>
                                <option value="">-เลือกโปรแกรมวิชา-</option>
                                <option value="เทคโนโลยีสารสนเทศ">เทคโนโลยีสารสนเทศ</option>
                                <option value="วิทยาการคอมพิวเตอร์">วิทยาการคอมพิวเตอร์</option>
                                <option value="วิศวกรรมซอฟแวร์">วิศวกรรมซอฟแวร์</option>
                            </select>
                        </div>
                        <div class="col">
                            รหัสนักศึกษา
                            <input type="text" name="std_code" class="form-control" placeholder="ขั้นต่ำ 5 ตัว" required
                                minlength="5" maxlength="20">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col col-sm-2">
                            คำนำหน้าชื่อ
                            <select name="std_firstname" class="form-control" required>
                                <option value="">-คำนำหน้าชื่อ-</option>
                                <option value="นาย">นาย</option>
                                <option value="นางสาว">นางสาว</option>
                            </select>
                        </div>
                        <div class="col col-sm-4">
                            ชื่อ
                            <input type="text" name="std_name" class="form-control" placeholder="ชื่อ" required>
                        </div>
                        <div class="col col-sm-6">
                            นามสกุล
                            <input type="text" name="std_lastname" class="form-control" placeholder="นามสกุล" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            เบอร์โทร
                            <input type="tel" name="std_phone" class="form-control" placeholder="เบอร์โทร" required
                                minlength="10" maxlength="10">
                        </div>
                        <div class="col">
                            อีเมล
                            <input type="email" name="std_email" class="form-control" placeholder="อีเมล" required>
                        </div>
                    </div>
                    <div class="d-grid gap-2 col-12 mx-auto">
                        <button class="btn btn-primary" type="submit">บันทึก</button>
                    </div>
                </form>
                <?php } ?>
                <br>
                <h5>ข้อมูลนักศึกษา
                    <a href="formAddSTD.php?act=add" class="btn btn-success btn-sm">+เพิ่มข้อมูล</a>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped  table-hover table-responsive table-bordered">
                        <thead>
                            <tr class="table-danger">
                                <th width="20%">รหัสนักศึกษา</th>
                                <th width="20%">ชื่อ-สกุล</th>
                                <th width="20%">โปรแกรม</th>
                                <th width="20%">เบอร์โทร</th>
                                <th width="20%">อีเมล</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //เรียกไฟล์เชื่อมต่อฐานข้อมูล
                            require_once 'connect.php';
                            //คิวรี่ข้อมูลมาแสดงในตาราง
                            $stmt = $conn->prepare("SELECT* FROM tbl_std ORDER BY std_code DESC");
                            $stmt->execute();
                            $result = $stmt->fetchAll();
                            foreach ($result as $row) {
                                ?>
                            <tr>
                                <td>
                                    <?= $row['std_code']; ?>
                                </td>
                                <td>
                                    <?= $row['std_firstname'] . $row['std_name'] . ' ' . $row['std_lastname']; ?>
                                </td>
                                <td>
                                    <?= $row['std_program']; ?>
                                </td>
                                <td>
                                    <?= $row['std_phone']; ?>
                                </td>
                                <td>
                                    <?= $row['std_email']; ?>
                                </td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <center>Basic PHP PDO Form add Student by devbanban.com 2022</center>
            </div>
        </div>
    </div>
</body>

</html>
<?php
//   echo '<pre>';
//       print_r($_POST);
// echo '</pre>';
// exit();
//ตรวจสอบตัวแปรที่ส่งมาจากฟอร์ม
if (isset($_POST['std_code']) && isset($_POST['std_program']) && isset($_POST['std_phone'])) {
    //ไฟล์เชื่อมต่อฐานข้อมูล
    require_once 'connect.php';
    //sql insert
    $stmt = $conn->prepare("INSERT INTO tbl_std
  (
  std_code,
  std_program, 
  std_firstname, 
  std_name, 
  std_lastname,
  std_phone,
  std_email
  )
  VALUES
  (
  :std_code,
  :std_program, 
  :std_firstname, 
  :std_name, 
  :std_lastname,
  :std_phone,
  :std_email
  )
  ");
    //bindParam data type
    $stmt->bindParam(':std_code', $_POST['std_code'], PDO::PARAM_STR);
    $stmt->bindParam(':std_program', $_POST['std_program'], PDO::PARAM_STR);
    $stmt->bindParam(':std_firstname', $_POST['std_firstname'], PDO::PARAM_STR);
    $stmt->bindParam(':std_name', $_POST['std_name'], PDO::PARAM_STR);
    $stmt->bindParam(':std_lastname', $_POST['std_lastname'], PDO::PARAM_STR);
    $stmt->bindParam(':std_phone', $_POST['std_phone'], PDO::PARAM_STR);
    $stmt->bindParam(':std_email', $_POST['std_email'], PDO::PARAM_STR);
    $result = $stmt->execute();
    $conn = null; //close connect db
    //เงื่อนไขตรวจสอบการเพิ่มข้อมูล
    if ($result) {
        echo '<script>
    setTimeout(function() {
      swal({
      title: "เพิ่มข้อมูลสำเร็จ",
      type: "success"
      }, function() {
      window.location = "formAddSTD.php"; //หน้าที่ต้องการให้กระโดดไป
      });
    }, 1000);
  </script>';
    } else {
        echo '<script>
    setTimeout(function() {
      swal({
      title: "เกิดข้อผิดพลาด",
      type: "error"
      }, function() {
      window.location = "formAddSTD.php"; //หน้าที่ต้องการให้กระโดดไป
      });
    }, 1000);
  </script>';
    } //else ของ if result

} //isset

//ตัวอย่าง workshop ป้องกันบันทึกข้อมูลซ้ำ https://devbanban.com/?p=4221
//ขอแนะนำระบบพร้อมใช้ ราคาเบาๆ ซื้อแล้วได้ code ทั้งหมด เอาไปต่อยอดได้ 
//ระบบแจ้งซ่อม https://www.youtube.com/watch?v=bkYBKfFauB8
//ระบบจัดการหอพัก https://www.youtube.com/watch?v=O7Bh0BNCDz8
//ระบบสอบออนไลน์ https://www.youtube.com/watch?v=WOTLFEflkMk&t=125s
?>